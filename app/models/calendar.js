import DS from 'ember-data';

export default DS.Model.extend({
  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),
  teacher: DS.belongsTo('teacher'),
  user: DS.belongsTo('user'),
  locations: DS.hasMany('location'),
  dateStart: DS.attr('date'),
  dateEnd: DS.attr('date')
});
