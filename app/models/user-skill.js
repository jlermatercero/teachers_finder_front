import DS from 'ember-data';

export default DS.Model.extend({
  skill: DS.belongsTo('skill'),
  user: DS.belongsTo('user'),
  experience: DS.attr('number')
});
