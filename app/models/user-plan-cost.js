import DS from 'ember-data';

export default DS.Model.extend({
  plan: DS.belongsTo('plan'),
  user: DS.belongsTo('user'),
  cost: DS.belongsTo('number')
});