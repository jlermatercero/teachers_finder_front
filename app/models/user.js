import DS from 'ember-data';

export default DS.Model.extend({
  email: DS.attr('string'),
  passwordDigest: DS.attr('string'),
  authToken: DS.attr('string'),
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  votesCount: DS.attr('number'),
  calendars: DS.hasMany('calendar'),
  votes: DS.hasMany('vote'),
  userSkills: DS.hasMany('userSkill'),
  userPlanCosts: DS.hasMany('userPlanCost')

  /*image*/
});
