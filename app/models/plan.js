import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  userPlanCosts: DS.attr('userPlanCost')
});
