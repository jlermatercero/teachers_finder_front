import DS from 'ember-data';

export default DS.Model.extend({
  street: DS.attr('string'),
  number: DS.attr('string'),
  neighborhood: DS.attr('string'),
  city: DS.attr('string'),
  state: DS.attr('string'),
  zipCode: DS.attr('string'),
  country: DS.attr('string'),
  lat: DS.attr('string'),
  lon: DS.attr('string'),
  calendar: DS.belongsTo('calendar')
});
